## Usage 

### Creating Backups

Backups can be create with 

```bash
docker volume create restic_backup
```

```bash
docker-compose exec mongo mongodump --host mongo:27017
```



```bash
docker run --rm -ti \
    -v restic_backup:/srv/restic-repo \
    -e RESTIC_REPOSITORY=/srv/restic-repo \
    -e RESTIC_PASSWORD=changeme \
    restic/restic init
```

```bash
docker run --rm -ti \
    -v docker_compose-rocketchat_mongo-dump-volume:/data:ro \
    -v restic_backup:/srv/restic-repo \
    -e RESTIC_REPOSITORY=/srv/restic-repo \
    -e RESTIC_PASSWORD=changeme \
    restic/restic backup /data
```

```bash
docker run --rm -ti \
    -v docker_compose-rocketchat_mongo-dump-volume:/data \
    -v restic_backup:/srv/restic-repo \
    -e RESTIC_REPOSITORY=/srv/restic-repo \
    -e RESTIC_PASSWORD=changeme \
    restic/restic restore latest --target /
```

For Restoring a dump

```bash
docker-compose exec mongo mongorestore /dump
```